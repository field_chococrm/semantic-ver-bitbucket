## [2.0.2](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/compare/v2.0.1...v2.0.2) (2020-06-08)


### Bug Fixes

* issue ([4ab7509](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/4ab75095daf76325c1421efd0fcf6721ea3ea167))

## [2.0.1](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/compare/v2.0.0...v2.0.1) (2020-06-08)


### Bug Fixes

* fixfixfixfixfix ([02af640](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/02af640ad94949e98885cec98d082584f155eb48))

# [2.0.0](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/compare/v1.1.1...v2.0.0) (2020-06-08)


* final ([8f8a51e](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/8f8a51e57f9547ee4da69ca637c82cad6d1e15d5))


### BREAKING CHANGES

* final

## [1.1.1](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/compare/v1.1.0...v1.1.1) (2020-06-08)


### Bug Fixes

* somthing ([ab97c53](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/ab97c5392029a45349460879c194f40de57b253f))

# [1.1.0](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/compare/v1.0.0...v1.1.0) (2020-06-08)


### Features

* add . in data ([8ee8e90](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/8ee8e90a91fe7f9043b36e459f3f0a6b3971aee4))

# 1.0.0 (2020-06-08)


### Bug Fixes

* ffff ([c91c75c](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/c91c75c96445f7e7fb7ca6fd40b1a5d3d578131d))
* private false ([1f72b34](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/1f72b34bcea6656052699ccd0bfb9fda7afc56ac))


### Features

* add permission ([2fdede4](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/2fdede41578a87700bd1ec2b0b0c2d60e6bf4187))
* add somthing ([7d68b34](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/7d68b34c8be08bb2c7e343f07808e2197f11044c))
* efrwafejklwfalefawefoawjefa wef ([468dfb8](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/468dfb8c7466577e4fbd71e1b45350016a1cb4b3))
* fewfewf ([618b6cd](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/618b6cd0c893fbd4d08c71b0f3898dc32232adb7))
* fff ([0907603](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/0907603debd893f19a433cfeff157d80a0eae2ad))
* ffff ([adc234e](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/adc234e08f50e67a78e0284b2b3bbea1fae7ee3d))
* fix ([cc7d20a](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/cc7d20aa9fa67417a8fbc5649c752025bc4a8190))
* fix ([bc29479](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/bc29479facd74fc1f14b2a698399b9a538d14d05))
* fix ver.node module ([bd18a64](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/bd18a6448971e899270b15f51d02da00cc3727fe))
* gfagafaefa ([74ed2bd](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/74ed2bd1c61ddcf16cfad475738702c82c3ee0ea))
* init git cz ([8084926](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/80849265717e5cb0e064682f153466cb4998d731))
* wefwefawefawefawef ([62bb795](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/62bb7954b6a69baec1f33f04187d85f2861392b9))
* werwerewrre ([5d4a87f](http://bitbucket.org/field_chococrm/semantic-ver-bitbucket/commits/5d4a87f70c93abafd0f3a9d5d6333a594edf9342))
